#!/bin/bash
SENDMAIL=/usr/sbin/sendmail
GPGIT=/opt/gpgit/gpgit.pl

#encrypt and resend directly from stdin
set -o pipefail
${GPGIT} "$4" | ${SENDMAIL} "$@"

exit $?
